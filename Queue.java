public final class Queue {
	public int[] data;
	private int size;
	private int capacity;
	private int front;
	private int rear;

	Queue() {
		this(10);
	}

	Queue(int capacity) {
		this.data = new int[capacity];
		this.capacity = capacity;
		this.size = 0;
		this.front = -1;
		this.rear = -1;
	}

	private boolean isEmpty() {
		if (this.front == -1) {
			return true;
		}
		return false;
	}

	private boolean isFull() {
		if (this.rear == this.capacity - 1) {
			return true;
		}
		return false;
	}

	private void reSize() {
		int newCapacity = this.capacity * 2;
		int[] newData = new int[newCapacity];
		for (int i = 0; i < this.capacity; i++) {
			newData[i] = this.data[i];
		}
		this.capacity = newCapacity;
		this.data = newData;
	}

	public void enQueue(int data) {
		if (this.isFull()) {
			this.reSize();
		}
		if (this.front == -1) {
			this.front++;
		}
		this.rear++;
		this.data[this.rear] = data;
		this.length++;
	}

	public int peekFront() {
		if (!this.isEmpty()) {
			return this.data[this.front];
		}
		return -1;
	}

	public int peekRear() {
		if (!this.isEmpty()) {
			return this.data[this.rear];
		}
		return -1;
	}

	public void deQueue() {
		if (!this.isEmpty()) {
			if (this.front == this.rear) {
				this.front = -1;
				this.rear = -1;
			} else {
				for (int i = this.front; i <= this.rear; i++) {
					this.data[i - 1] = this.data[i];
				}
				this.rear--;
			}
		}
	}

	public int getSize() {
		return this.size;
	}

}
