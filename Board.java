/* Java Program for Tic Tac Toe Game*/

import java.util.Scanner;

public class Board {
    public int arr[][] = new int[3][3];

    Board() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                arr[i][j] = 0;
            }
        }
    }

    public void display() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(" " + arr[i][j] + " ");
            }
            System.out.println(" ");
        }
    }

    public boolean hasWon(int value) {
        int a = 0, b = 0, c = 0, d = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (arr[i][j] == value) {
                    a++;
                } else if (arr[j][i] == value) {
                    b++;
                }
            }
            if (a == 3 || b == 3) {
                return true;
            } else {
                a = 0;
                b = 0;
            }
            if (arr[i][i] == value) {
                c++;
            } else if (arr[i][2 - i] == value) {
                d++;
            }
        }
        if (c == 3 || d == 3) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Board table = new Board();
        int player1 = 1;
        int player2 = 2;
        boolean won = false;
        int winner = 3;
        int player = player1;
        int count = 0;
        table.display();
        while (!won && count != 9) {
            System.out.print("Player " + player + " : ");
            int x = scan.nextInt() - 1;
            int y = scan.nextInt() - 1;
            if (x >= 0 && x < 3 && y >= 0 && y < 3) {
                if (table.arr[x][y] == 0) {
                    table.arr[x][y] = player;
                    count++;
                } else {
                    continue;
                }
                table.display();
                if (table.hasWon(player)) {
                    won = true;
                    winner = player;
                    break;
                }

            } else {
                continue;
            }
            player = (player == player1) ? player2 : player1;
        }
        if (count == 9) {
            System.out.print("Game has draw");
        } else {
            System.out.print("Player " + winner + " has won");
        }
        scan.close();
    }
}