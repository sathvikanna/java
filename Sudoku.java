import java.util.*;
import java.lang.Math;

public class Sudoku {
    public int[][] grid = { { 8, 1, 2, 7, 5, 3, 6, 4, 9 }, { 9, 4, 3, 6, 8, 2, 1, 7, 5 }, { 6, 7, 5, 4, 9, 1, 2, 8, 3 },
            { 1, 5, 4, 2, 3, 7, 8, 9, 6 }, { 3, 6, 9, 8, 4, 5, 7, 2, 1 }, { 2, 8, 7, 1, 6, 9, 5, 3, 4 },
            { 5, 2, 1, 9, 7, 4, 3, 6, 8 }, { 4, 3, 8, 5, 2, 6, 9, 1, 7 }, { 7, 9, 6, 3, 1, 8, 4, 5, 2 } };

    Sudoku(int[][] grid) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                this.grid[i][j] = grid[i][j];
            }
        }
        System.out.println("Sudoku Grid is created");
        this.display();
    }

    Sudoku(int level) {
        this.randomSudoku(level);
        System.out.println("We generated a Sudoku for you");
        this.display();
    }

    public void randomSudoku(int level) {
        int value = 0;
        switch (level) {
            case 1:
                value = 20;
                break;
            case 2:
                value = 30;
                break;
            case 3:
                value = 40;
                break;
            case 4:
                value = 50;
                break;
            case 5:
                value = 60;
                break;
            default:
                value = 57;
                break;
        }
        ArrayList<Integer> randPos = randChoices(81);
        for (int i = 0; i < value; i++) {
            int x = (int) ((randPos.get(i) - 1) / 9);
            int y = (int) ((randPos.get(i) - 1) % 9);
            this.grid[x][y] = 0;
        }
    }

    public void display() {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                System.out.print(grid[i][j]);
                System.out.print(" ");
            }
            System.out.println(" ");
        }
    }

    public boolean isSolved() {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (this.grid[i][j] == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public int getNum(ArrayList<Integer> v) {
        int n = v.size();
        int index = (int) (Math.random() * n);
        int num = v.get(index);
        v.set(index, v.get(n - 1));
        v.remove(n - 1);
        return num;
    }

    public ArrayList<Integer> randChoices(int n) {
        ArrayList<Integer> a = new ArrayList<>(n);
        ArrayList<Integer> b = new ArrayList<>(n);
        for (int i = 0; i < n; i++)
            a.add(i + 1);
        while (a.size() > 0) {
            b.add(getNum(a));
        }
        return b;
    }

    public boolean isValid(int xIndex, int yIndex, int number) {
        if (xIndex < 0 || xIndex > 9 || yIndex < 0 || yIndex > 9 || number < 1 || number > 9) {
            return false;
        }
        return true;
    }

    public boolean evaluate(int xIndex, int yIndex, int number) {
        if (isValid(xIndex, yIndex, number)) {
            for (int i = 0; i < 9; i++) {
                if (i != xIndex && grid[i][yIndex] == number) {
                    return false;
                }
                if (i != yIndex && grid[xIndex][i] == number) {
                    return false;
                }
            }
            int r = xIndex - xIndex % 3;
            int c = yIndex - yIndex % 3;

            for (int i = r; i < r + 3; i++) {
                for (int j = c; j < c + 3; j++) {
                    if (grid[i][j] == number)
                        return false;
                }
            }
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter level 1 - 5 ");
        int level = scanner.nextInt();
        Sudoku sudoku = new Sudoku(level);
        while (!sudoku.isSolved()) {
            System.out.println("Enter x and y index of Sudoku and n to fill it with");
            int x = scanner.nextInt() - 1;
            int y = scanner.nextInt() - 1;
            int n = scanner.nextInt();
            if (sudoku.evaluate(x, y, n)) {
                sudoku.grid[x][y] = n;
            } else {
                System.out.println("Invalid");
            }
            sudoku.display();
        }
        scanner.close();
    }
}