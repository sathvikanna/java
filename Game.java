import java.util.*;
import java.lang.Math;

public class Game {
    int[][] boardTiles = new int[4][4];
    int zeroIndexI = -1;
    int zeroIndexJ = -1;

    Game() {
        ArrayList<Integer> boardValues = generateRandom(16);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                boardTiles[i][j] = boardValues.get(i * 4 + j);
            }
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (boardTiles[i][j] == 0) {
                    zeroIndexI = i;
                    zeroIndexJ = j;
                }
            }
        }
    }

    public static int getNum(ArrayList<Integer> v) {
        int n = v.size();
        int index = (int) (Math.random() * n);
        int num = v.get(index);
        v.set(index, v.get(n - 1));
        v.remove(n - 1);
        return num;
    }

    public static ArrayList<Integer> generateRandom(int n) {
        ArrayList<Integer> a = new ArrayList<>(n);
        ArrayList<Integer> b = new ArrayList<>(n);
        for (int i = 0; i < n; i++)
            a.add(i);
        while (a.size() > 0) {
            b.add(getNum(a));
        }
        return b;
    }

    public void display() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.format("%02d", boardTiles[i][j]);
                System.out.print(" ");
            }
            System.out.println("");
        }
    }

    public boolean hasWon() {
        boolean status = true;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (boardTiles[i][j] != ((i * 4 + j + 1) % 16)) {
                    status = false;
                }
            }
        }
        return status;
    }

    public boolean canSwap(int indexI, int indexJ) {
        if (indexI >= 0 && indexJ <= 3 && indexI <= 3 && indexJ >= 0) {
            int[] x = { 0, 0, +1, -1 };
            int[] y = { +1, -1, 0, 0 };
            for (int i = 0; i < 4; i++) {
                int tempI = zeroIndexI + x[i];
                int tempJ = zeroIndexJ + y[i];
                if (indexI == tempI && indexJ == tempJ) {
                    return true;
                }
            }
        }
        return false;

    }

    public void swap(int indexI, int indexJ) {
        if (canSwap(indexI, indexJ)) {
            int temp = boardTiles[indexI][indexJ];
            boardTiles[indexI][indexJ] = 0;
            boardTiles[zeroIndexI][zeroIndexJ] = temp;
            zeroIndexI = indexI;
            zeroIndexJ = indexJ;
        } else {
            System.out.println("Invalid Input");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Game gameBoard = new Game();
        gameBoard.display();
        while (!gameBoard.hasWon()) {
            System.out.print("Enter the position in one indexing : ");
            int i = scanner.nextInt();
            int j = scanner.nextInt();
            gameBoard.swap(i - 1, j - 1);
            gameBoard.display();
        }
        System.out.println("You have won");
        scanner.close();
    }
}