public final class Stack {
	private int[] data;
	public int capacity;
	private int top;
	public int size;

	Stack() {
		this(10);
	}

	Stack(int capacity) {
		this.data = new int[capacity];
		this.capacity = capacity;
		this.top = -1;
		this.size = 0;
	}

	private boolean isEmpty() {
		if (this.top == -1) {
			return true;
		}
		return false;
	}

	private boolean isFull() {
		if (this.top == this.capacity - 1) {
			return true;
		}
		return false;
	}

	private void reSize() {
		int newCapacity = this.capacity * 2;
		int[] newData = new int[newCapacity];
		for (int i = 0; i < this.capacity; i++) {
			newData[i] = this.data[i];
		}
		this.capacity = newCapacity;
		this.data = newData;
	}

	public void push(int data) {
		if (this.isFull()) {
			this.reSize();
		}
		this.top++;
		this.data[this.top] = data;
		this.size++;
	}

	public int pop() {
		if (!this.isEmpty()) {
			int data = this.data[this.top];
			this.top--;
			this.size--;
			return data;
		}
		return -1;
	}

	public int peek() {
		if (!this.isEmpty()) {
			return this.data[this.top];
		}
		return -1;
	}

	public int getSize() {
		return this.size;
	}
}